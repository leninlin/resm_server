FROM ubuntu:14.04
MAINTAINER Vladimir Akritskiy <lenin.lin@gmail.com>

RUN apt-get update && apt-get install -y libevent-dev g++ make

ADD ./ /app/
RUN cd /app && make && make install

CMD ["/usr/local/bin/resm", "-s", "0.0.0.0"]
