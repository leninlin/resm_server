##Resm

Resm is a service for allocating resources on demand. Resources are requested via HTTP requests.


###Requirements

To build the applications required installed `libevent` and `g++`. To test need to install Google Test Framework.
```
$ sudo apt-get install -y libevent-dev g++
```


###Download

Clone repository via GIT
```
$ git clone git@bitbucket.org:leninlin/resm_server.git
$ cd resm_server
```
or download .tar file via WGET
```
$ wget https://bitbucket.org/leninlin/resm_server/get/master.tar.gz
$ tar -zxf master.tar.gz
$ cd ./leninlin-resm_server*
```


###Build

Build server you can by using the `make` utility
```
$ make
```

Start the application:
```
$ ./resm
```
or
```
$ make run
```


###Build DEB/RPM package

Build DEB package
```
$ make deb
$ dpkg -i resm_*.deb
```

Or build RPM package
```
$ make rpm
$ rpm -i resm_*.rpm
```


###Build docker container

Build container
```
$ sudo make docker
```

And run it
```
$ sudo docker run -d -p 8080:8080 --name resm leninlin/resm
```


###Install

For install the application you can run commands
```
$ make
$ sudo make install
```

Resm installed as a service and will run the command
```
$ sudo service resm start
```

###Configure


Resm gets settings as parameters. View help for more information
```
$ resm -h
```

Else settings can be registered in the ini file `/etc/resm/resm.ini`


###Test

To perform the test will need to install Google Test Framework. For Ubuntu can be most easily using [this instruction](http://www.eriksmistad.no/getting-started-with-google-test-on-ubuntu/).

Build tests performed by the `make`
```
$ make test
```

And run by
```
$ ./resm_test
```
or
```
$ make test_run
```
