#include "http_server.h"

evutil_socket_t Socket = -1;
bool volatile IsRun = true;

const char *SrvAddress;
short SrvPort;

void (* OnRequest)(struct evhttp_request *, void *);

void StartServer(const char *address, short port, void (*OnRequestFunc)(struct evhttp_request *, void *))
{
    OnRequest = OnRequestFunc;
    SrvAddress = address;
    SrvPort = port;
    IsRun = true;
    Socket = -1;

    auto ThreadDeleter = [&] (std::thread *t) { IsRun = false; t->join(); delete t; };
    typedef std::unique_ptr<std::thread, decltype(ThreadDeleter)> ThreadPtr;
    typedef std::vector<ThreadPtr> ThreadPool;
    ThreadPool Threads;
    for (int i = 0 ; i < SRV_THREAD_COUNT ; ++i)
    {
      ThreadPtr Thread(new std::thread(ThreadFunc), ThreadDeleter);
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      Threads.push_back(std::move(Thread));
    }
    std::cout << "Listen port: " << SrvPort << std::endl;

    while(1){
        sleep(100);
    }
    return;
}

void ThreadFunc ()
{
    std::unique_ptr<event_base, decltype(&event_base_free)> EventBase(event_base_new(), &event_base_free);
    if (!EventBase) {
        throw std::runtime_error("Failed to create new base_event.");
    }

    std::unique_ptr<evhttp, decltype(&evhttp_free)> EvHttp(evhttp_new(EventBase.get()), &evhttp_free);
    if (!EvHttp) {
        throw std::runtime_error("Failed to create new evhttp.");
    }

    evhttp_set_gencb(EvHttp.get(), OnRequest, nullptr);
    if (Socket == -1) {
        auto *BoundSock = evhttp_bind_socket_with_handle(EvHttp.get(), SrvAddress, SrvPort);
        if (!BoundSock)
            throw std::runtime_error("Failed to bind server socket.");
        if ((Socket = evhttp_bound_socket_get_fd(BoundSock)) == -1)
            throw std::runtime_error("Failed to get server socket for next instance.");
    } else {
        if (evhttp_accept_socket(EvHttp.get(), Socket) == -1)
            throw std::runtime_error("Failed to bind server socket for new instance.");
    }
    for ( ; IsRun ; ) {
        event_base_loop(EventBase.get(), EVLOOP_NONBLOCK);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
