#ifndef H_HTTP_SERVER
#define H_HTTP_SERVER

#include <iostream>
#include <memory>
#include <chrono>
#include <thread>
#include <cstdint>
#include <vector>
#include <evhttp.h>
#include <unistd.h>

#define SRV_THREAD_COUNT 4

void StartServer(const char *, short, void (*)(struct evhttp_request *, void *));
void ThreadFunc();

#endif