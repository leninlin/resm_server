#include "app.h"

int countResources;
std::vector<CResource*> listResources;
const char * routes[4] = {"allocate", "deallocate", "list", "reset"};

void StartApp(int count)
{
    char name[10];
    countResources = count;
    listResources.reserve(countResources);
    for (int i = 0; i < countResources; i++) {
        sprintf(name, "r%d", i);
        listResources[i] = new CResource(name);
    }
}

void Router(struct evhttp_request * req, void * arg)
{
    if (req->type != EVHTTP_REQ_GET) {
        SendResponse(req, HTTP_BADREQUEST, "Bad request.");
        return;
    }

    Response res;
    switch (CheckRoutes(req->uri)) {
        case 0: res = AllocateRoute(req->uri); break;
        case 1: res = DeallocateRoute(req->uri); break;
        case 2: res = ListRoute(req->uri); break;
        case 3: res = ResetRoute(req->uri); break;

        default:
            res.code = HTTP_BADREQUEST;
            res.message = "Bad request.";
    }

    SendResponse(req, res.code, res.message);
    return;
}

void SendResponse(struct evhttp_request * req, int code, const char * message)
{
    struct evbuffer *evb = evbuffer_new();
    if (!evb) { return; }

    const char * code_message;
    switch (code) {
        case 200: code_message = "OK"; break;
        case 201: code_message = "Created"; break;
        case 204: code_message = "No Content"; break;
        case 400: code_message = "Bad Request"; break;
        case 404: code_message = "Not Found"; break;
        case 503: code_message = "Service Unavailable"; break;
        default: code_message = "";
    }

    evbuffer_add_printf(evb, "%s", message);
    evhttp_add_header(req->output_headers, "Connection", "close");
    evhttp_send_reply(req, code, code_message, evb);
    evbuffer_free(evb);
}

int CheckRoutes(char * uri)
{
    int result = -1;

    if (strncmp(uri, "/", 1) == 0) {
        uri++;
    }

    for (uint i = 0; i < sizeof(routes)/sizeof(routes[0]); i++) {
        if (strcmp(uri, routes[i]) == 0) {
            result = i;
            break;
        }
        if (strncmp(uri, routes[i], strlen(routes[i])) == 0 && strncmp(uri + strlen(routes[i]), "/", 1) == 0) {
            result = i;
            break;
        }
    }

    return result;
}

CResource* GetFreeResource() {
    CResource* res = nullptr;
    for (int i = 0; i < countResources; i++) {
        if (listResources[i]->IsBusy() == false) {
            res = listResources[i];
            break;
        }
    }

    return res;
}

CResource* GetResourceByName(char * name) {
    CResource* res = nullptr;
    for (int i = 0; i < countResources; i++) {
        if (strcmp(listResources[i]->GetName(), name) == 0) {
            res = listResources[i];
            break;
        }
    }

    return res;
}

Response AllocateRoute(char * uri) {
    Response response;

    if (strncmp(uri+1, routes[0], strlen(routes[0])) != 0 || strlen(uri) <= strlen(routes[0]) + 2) {
        response.code = HTTP_BADREQUEST;
        response.message = "Bad request.";
        return response;
    }

    CResource* res;
    res = GetFreeResource();
    if (res == nullptr) {
        response.code = HTTP_SERVUNAVAIL;
        response.message = "Out of resources.";
        return response;
    }

    char * nameAllocate;
    nameAllocate = uri + strlen(routes[0]) + 2;
    if (res->Assignee(nameAllocate)) {
        response.code = 201;
        response.message = res->GetName();
    } else {
        response.code = HTTP_SERVUNAVAIL;
        response.message = "Failure to allocate resources.";
    }

    return response;
}

Response DeallocateRoute(char * uri) {
    Response response;

    if (strncmp(uri+1, routes[1], strlen(routes[1])) != 0 || strlen(uri) <= strlen(routes[1]) + 2) {
        response.code = HTTP_BADREQUEST;
        response.message = "Bad request.";
        return response;
    }

    char * nameDeallocate;
    nameDeallocate = uri + strlen(routes[1]) + 2;

    CResource* res;
    res = GetResourceByName(nameDeallocate);
    if (res == nullptr || res->IsBusy() == false) {
        response.code = HTTP_NOTFOUND;
        response.message = "Not allocated.";
        return response;
    }

    res->Free();

    response.code = 204;
    response.message = "";
    return response;
}

Response ListRoute(char * uri) {
    Response response;
    json result;

    if (strncmp(uri+1, routes[2], strlen(routes[2])) != 0) {
        response.code = HTTP_BADREQUEST;
        response.message = "Bad request.";
        return response;
    }

    if (strlen(uri) <= strlen(routes[2]) + 2) {
        result["allocated"] = json::object();
        result["deallocated"] = json::array();

        for (int i = 0; i < countResources; i++) {
            if (listResources[i]->IsBusy()) {
                result["allocated"][listResources[i]->GetName()] = listResources[i]->GetOwner();
            } else {
                result["deallocated"].push_back(listResources[i]->GetName());
            }
        }
    } else {
        char * nameList = nullptr;
        nameList = uri + strlen(routes[2]) + 2;

        result = json::array();
        
        for (int i = 0; i < countResources; i++) {
            if (listResources[i]->IsBusy()) {
                if (strcmp(listResources[i]->GetOwner(), nameList) == 0) {
                    result.push_back(listResources[i]->GetName());
                }
            }
        }
    }

    response.code = HTTP_OK;
    response.message = result.dump().c_str();
    return response;
}

Response ResetRoute(char * uri) {
    Response response;

    if (strncmp(uri+1, routes[3], strlen(routes[3])) != 0) {
        response.code = HTTP_BADREQUEST;
        response.message = "Bad request.";
        return response;
    }

    for (int i = 0; i < countResources; i++) {
        listResources[i]->Free();
    }

    response.code = 204;
    response.message = "";
    return response;
}