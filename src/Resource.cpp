#include "Resource.h"

CResource::CResource(char * _name)
{
    name = (char*)malloc(sizeof(_name));
    strcpy(name, _name);
    allocated = false;
}

bool CResource::Assignee(const char * _owner)
{
    return Assignee(const_cast<char*>(_owner));
}

bool CResource::Assignee(char * _owner)
{
    if (allocated == true) {
        return false;
    }

    owner = (char*)malloc(sizeof(_owner));
    strcpy(owner, _owner);
    allocated = true;
    return true;
}

void CResource::Free()
{
    allocated = false;
}

const char* CResource::GetName()
{
    return const_cast<const char*>(name);
}

char* CResource::GetOwner()
{
    if (allocated == false) {
        return nullptr;
    }

    return owner;
}

bool CResource::IsBusy()
{
    return allocated;
}
