#ifndef H_RESOURCE
#define H_RESOURCE

#include <stdlib.h>
#include <cstring>

class CResource
{

    bool allocated;
    char * name;
    char * owner;

public:
    CResource(char *);

    bool Assignee(char *);
    bool Assignee(const char *);
    void Free();

    const char* GetName();
    char* GetOwner();
    bool IsBusy();
};

#endif