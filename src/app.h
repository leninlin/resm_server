#pragma once

#ifndef H_APP
#define H_APP

#include "config.h"
#include "http_server.h"
#include "Resource.h"

#include <string.h>
#include <vector>

#include "../lib/json.hpp"

using json = nlohmann::json;

typedef struct {
    int code;
    const char * message;
} Response;

void StartApp(int);
void Router(struct evhttp_request *, void *);
void SendResponse(struct evhttp_request *, int, const char *);
int CheckRoutes(char *);
CResource* GetFreeResource();

Response AllocateRoute(char *);
Response DeallocateRoute(char *);
Response ListRoute(char *);
Response ResetRoute(char *);

#endif