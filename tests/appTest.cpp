#include "../src/app.h"

#include "gtest/gtest.h"

TEST(App, CheckRoutes)
{
    ASSERT_EQ(CheckRoutes(const_cast<char*>("/list")), 2);
    ASSERT_EQ(CheckRoutes(const_cast<char*>("/list/")), 2);
    ASSERT_EQ(CheckRoutes(const_cast<char*>("/list/test")), 2);
    ASSERT_EQ(CheckRoutes(const_cast<char*>("/listing")), -1);
    ASSERT_EQ(CheckRoutes(const_cast<char*>("/test/list")), -1);
}

TEST(App, AllocateRoute)
{
    StartApp(3);

    Response res;
    res = AllocateRoute(const_cast<char*>("/allocate/"));
    ASSERT_EQ(res.code, 400);
    ASSERT_EQ(res.message, "Bad request.");

    res = AllocateRoute(const_cast<char*>("/dealloca/asd"));
    ASSERT_EQ(res.code, 400);
    ASSERT_EQ(res.message, "Bad request.");

    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_EQ(res.code, 201);
    ASSERT_STREQ(res.message, "r0");

    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_EQ(res.code, 201);
    ASSERT_STREQ(res.message, "r1");

    res = AllocateRoute(const_cast<char*>("/allocate/eva"));
    ASSERT_EQ(res.code, 201);
    ASSERT_STREQ(res.message, "r2");

    res = AllocateRoute(const_cast<char*>("/allocate/eva"));
    ASSERT_EQ(res.code, 503);
    ASSERT_STREQ(res.message, "Out of resources.");
}

TEST(App, DeallocateRoute)
{
    StartApp(3);

    Response res;

    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r0");
    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r1");
    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r2");

    res = DeallocateRoute(const_cast<char*>("/deallocate/"));
    ASSERT_EQ(res.code, 400);
    ASSERT_STREQ(res.message, "Bad request.");

    res = DeallocateRoute(const_cast<char*>("/deallocate/alice"));
    ASSERT_EQ(res.code, 404);
    ASSERT_STREQ(res.message, "Not allocated.");

    res = DeallocateRoute(const_cast<char*>("/deallocate/r1"));
    ASSERT_EQ(res.code, 204);
    ASSERT_STREQ(res.message, "");

    res = DeallocateRoute(const_cast<char*>("/deallocate/r1"));
    ASSERT_EQ(res.code, 404);
    ASSERT_STREQ(res.message, "Not allocated.");
}

TEST(App, ListRoute)
{
    StartApp(3);

    Response res;

    res = ListRoute(const_cast<char*>("/last"));
    ASSERT_EQ(res.code, 400);
    ASSERT_STREQ(res.message, "Bad request.");

    res = ListRoute(const_cast<char*>("/list"));
    ASSERT_EQ(res.code, 200);
    ASSERT_STREQ(res.message, "{\"allocated\":{},\"deallocated\":[\"r0\",\"r1\",\"r2\"]}");

    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r0");
    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r1");
    res = AllocateRoute(const_cast<char*>("/allocate/kate"));
    ASSERT_STREQ(res.message, "r2");
    res = DeallocateRoute(const_cast<char*>("/deallocate/r1"));
    ASSERT_EQ(res.code, 204);

    res = ListRoute(const_cast<char*>("/list"));
    ASSERT_EQ(res.code, 200);
    ASSERT_STREQ(res.message, "{\"allocated\":{\"r0\":\"alice\",\"r2\":\"kate\"},\"deallocated\":[\"r1\"]}");

    res = ListRoute(const_cast<char*>("/list/kate"));
    ASSERT_EQ(res.code, 200);
    ASSERT_STREQ(res.message, "[\"r2\"]");

    res = AllocateRoute(const_cast<char*>("/allocate/kate"));
    ASSERT_STREQ(res.message, "r1");

    res = ListRoute(const_cast<char*>("/list/kate"));
    ASSERT_EQ(res.code, 200);
    ASSERT_STREQ(res.message, "[\"r1\",\"r2\"]");

    res = DeallocateRoute(const_cast<char*>("/deallocate/r0"));

    res = ListRoute(const_cast<char*>("/list/alice"));
    ASSERT_EQ(res.code, 200);
    ASSERT_STREQ(res.message, "[]");
}

TEST(App, ResetRoute)
{
    StartApp(3);

    Response res;

    res = ResetRoute(const_cast<char*>("/risit"));
    ASSERT_EQ(res.code, 400);
    ASSERT_STREQ(res.message, "Bad request.");

    res = ListRoute(const_cast<char*>("/list"));
    ASSERT_STREQ(res.message, "{\"allocated\":{},\"deallocated\":[\"r0\",\"r1\",\"r2\"]}");

    res = ResetRoute(const_cast<char*>("/reset"));
    ASSERT_EQ(res.code, 204);

    res = ListRoute(const_cast<char*>("/list"));
    ASSERT_STREQ(res.message, "{\"allocated\":{},\"deallocated\":[\"r0\",\"r1\",\"r2\"]}");

    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r0");
    res = AllocateRoute(const_cast<char*>("/allocate/alice"));
    ASSERT_STREQ(res.message, "r1");
    res = AllocateRoute(const_cast<char*>("/allocate/kate"));
    ASSERT_STREQ(res.message, "r2");

    res = ListRoute(const_cast<char*>("/list"));
    ASSERT_STREQ(res.message, "{\"allocated\":{\"r0\":\"alice\",\"r1\":\"alice\",\"r2\":\"kate\"},\"deallocated\":[]}");

    res = ResetRoute(const_cast<char*>("/reset"));
    ASSERT_EQ(res.code, 204);

    res = ListRoute(const_cast<char*>("/list"));
    ASSERT_STREQ(res.message, "{\"allocated\":{},\"deallocated\":[\"r0\",\"r1\",\"r2\"]}");
}
