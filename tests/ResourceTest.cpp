#include "../src/Resource.h"

#include "gtest/gtest.h"

TEST(Resource, name)
{
    CResource res = CResource(const_cast<char*>("Res1"));

    ASSERT_STREQ(res.GetName(), "Res1");
}

TEST(Resource, assignee)
{
    CResource res = CResource(const_cast<char*>("Res1"));

    ASSERT_FALSE(res.IsBusy());

    ASSERT_TRUE(res.Assignee("Daniel"));
    ASSERT_TRUE(res.IsBusy());

    ASSERT_FALSE(res.Assignee("Nick"));
    ASSERT_TRUE(res.IsBusy());
}

TEST(Resource, free)
{
    CResource res = CResource(const_cast<char*>("Res1"));

    res.Assignee("Daniel");
    ASSERT_TRUE(res.IsBusy());
    
    res.Free();
    ASSERT_FALSE(res.IsBusy());
}

TEST(Resource, owner)
{
    char * owner;

    CResource res = CResource(const_cast<char*>("Res1"));

    owner = res.GetOwner();
    ASSERT_STREQ(owner, nullptr);

    res.Assignee("Daniel");
    owner = res.GetOwner();
    ASSERT_STREQ(owner, "Daniel");
}
