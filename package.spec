Name: resm
Version: 1.0
Release: 1%{?dist}
Summary: Resm service.

License: GPL
BuildRoot: %{_tmppath}/%{name}–%{version}–%{release}-root-%(%{__id_u} -n)


%description
Resm is a service for allocating resources on demand.

%build
make

%install
mkdir -p $RPM_BUILD_ROOT/etc/resm
mkdir -p $RPM_BUILD_ROOT/usr/local/bin
mkdir -p $RPM_BUILD_ROOT/etc/init.d

cp resm.ini $RPM_BUILD_ROOT/etc/resm/
cp resm $RPM_BUILD_ROOT/usr/local/bin/
cp ./service.sh $RPM_BUILD_ROOT/etc/init.d/resm

chmod +x $RPM_BUILD_ROOT/usr/local/bin/resm
chmod +x $RPM_BUILD_ROOT/etc/init.d/resm

%files
/
