#include "src/app.h"

#include "lib/ini.h"

#include <signal.h>

void signal_handler(int);
int handler(void*, const char*, const char*, const char*);

typedef struct
{
    int count;
    const char * host;
    int port;
} configuration;

int main(int argc, char **argv)
{
    configuration config;

    config.count = RESOURCES_COUNT;
    config.port = PORT;
    config.host = HOST;

    if (ini_parse("./resm.ini", handler, &config) < 0) {
        ini_parse("/etc/resm/resm.ini", handler, &config);
    }

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-h") == 0) {
            std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
            std::cout << "    -h: Print help." << std::endl;
            std::cout << "    -c <number>: Count resources. Deault " << RESOURCES_COUNT << std::endl;
            std::cout << "    -s <adress>: Host address. Default " << HOST << std::endl;
            std::cout << "    -p <number>: Port number. Default " << PORT << std::endl;
            return 1;
        }

        if (strcmp(argv[i], "-c") == 0) {
            i++;
            config.count = atoi(argv[i]);
            std::cout << "Count " << config.count << std::endl;
        }

        if (strcmp(argv[i], "-p") == 0) {
            i++;
            config.port = atoi(argv[i]);
            std::cout << "Port " << config.port << std::endl;
        }

        if (strcmp(argv[i], "-s") == 0) {
            i++;
            config.host = strdup(argv[i]);
            std::cout << "Host " << config.host << std::endl;
        }
    }

    signal(SIGINT, signal_handler);

    StartApp(config.count);
    StartServer(config.host, config.port, Router);
}

void signal_handler(int sig)
{
    exit(sig);
}

int handler(void* user, const char* section, const char* name, const char* value)
{
    configuration* pconfig = (configuration*)user;

    if (strcmp(name, "count") == 0) {
        pconfig->count = atoi(value);
    } else if (strcmp(name, "host") == 0) {
        pconfig->host = strdup(value);
    } else if (strcmp(name, "port") == 0) {
        pconfig->port = atoi(value);
    } else {
        return 0;
    }
    return 1;
}
