PROJECT = resm
VERSION = 1.0
REVISION = 1
ARCH = $(shell dpkg-architecture -qDEB_BUILD_ARCH)

DEB_PATH = $(PROJECT)_$(VERSION)-$(REVISION)_$(ARCH)

CXX = g++

LIBS = pthread event
FLAGS = -Wall -std=c++11  -O3 -g0

CPP = $(wildcard main.cpp lib/*.cpp src/*.cpp)
OBJ = $(CPP:.cpp=.o)

CPP_TESTS = $(wildcard lib/*.cpp src/*.cpp tests/*.cpp)
OBJ_TESTS = $(CPP_TESTS:.cpp=.o)

%.o: %.cpp
	$(CXX) $(FLAGS) -c $^ -o $@

all: $(OBJ)
	$(CXX) $(FLAGS) $(OBJ) -o $(PROJECT) $(addprefix -l, $(LIBS))

run:
	./${PROJECT}

install:
	mkdir -p /etc/${PROJECT}
	cp ${PROJECT}.ini /etc/${PROJECT}/
	cp ${PROJECT} /usr/local/bin/
	chmod +x /usr/local/bin/${PROJECT}
	cp ./service.sh /etc/init.d/${PROJECT}
	chmod +x /etc/init.d/${PROJECT}
	update-rc.d ${PROJECT} defaults

test: $(OBJ_TESTS)
	$(CXX) $(FLAGS) $(OBJ_TESTS) -o $(PROJECT)_test -lgtest $(addprefix -l, $(LIBS))

test_run:
	./${PROJECT}_test

deb: all
	mkdir -p $(DEB_PATH)/DEBIAN
	mkdir -p $(DEB_PATH)/etc/$(PROJECT)
	mkdir -p $(DEB_PATH)/etc/init.d
	mkdir -p $(DEB_PATH)/usr/local/bin

	cp $(PROJECT) $(DEB_PATH)/usr/local/bin/
	cp $(PROJECT).ini $(DEB_PATH)/etc/$(PROJECT)/
	cp service.sh $(DEB_PATH)/etc/init.d/$(PROJECT)
	chmod +x $(DEB_PATH)/etc/init.d/$(PROJECT)

	sed 's/<ARCH>/$(ARCH)/g' control > $(DEB_PATH)/DEBIAN/control
	dpkg-deb --build $(DEB_PATH)

rpm:
	mkdir -p ~/rpmbuild/BUILD
	cp -r ./* ~/rpmbuild/BUILD
	rpmbuild -v -bb package.spec
	mv ~/rpmbuild/RPMS/*/resm-*.rpm ./
	rm -rf ~/rpmbuild

docker:
	docker build -t leninlin/resm:latest .

clean:
	rm -rf ${PROJECT} $(PROJECT)_test $(PROJECT)_*/ *.deb *.rpm *.o src/*.o tests/*.o
